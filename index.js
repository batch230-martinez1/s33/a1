

// 3
fetch("https://jsonplaceholder.typicode.com/todos",
{
  method: "GET"
}
)
// 4
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title
	}))
	console.log(list);
});
// 5
fetch("https://jsonplaceholder.typicode.com/todos/1",
{
  method : "GET"
}
)
.then(res => res.json())
.then(response => {
  console.log(response)

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

  let title = response.title
  let status = response.completed
  console.log(`The item "${title}" on the list has a status of "${status}" `);
});

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos",
{
  method: "POST",
  headers:{
    "Content-Type" : "application/json"
  },
  body : JSON.stringify({
    userId: 202,
    id : 202,
    title: "Created to do list",
    completed : false
  })
  
})
.then(response => response.json())
.then(json => console.log(json))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
  method : "PUT",
/*   9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID */
  headers :{
    "Content-Type" : "application/json"
  },
  body : JSON.stringify({
    title : "Updated To Do List Item",
    description : "To update the my to do list with a different data structure",
    status : "Pending",
    dateCompleted : "Pending",
    userId: 1
  })

})
.then(response=> response.json())
.then(json => console.log(json))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",{
  method : "PATCH",
  headers : {
    "Content-Type" : "application/json"
  },
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
  body : JSON.stringify({
    status : "Complete",
    dateCompleted : "02/8/23"
  })
})
.then(response => response.json())
.then(json => console.log(json))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",
  {
    method : "DELETE",
  }
).then(response => response.json())
.then(json =>console.log(json))






